import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../view/busqueda.dart';

final FirebaseFirestore firestore = FirebaseFirestore.instance;
final CollectionReference collectionReference = firestore.collection('Users');


class ComprobarCamposRegistro {
  ComprobarCamposRegistro();

  Future<bool> checkNicknamelExists(String nickname) async {
    final usersRef = FirebaseFirestore.instance.collection('Users');
    final snapshot = await usersRef.where('nickname', isEqualTo: nickname).get();

    return snapshot.docs.isNotEmpty;
  }

  Future<bool> checkEmailExists(String email) async {
    final usersRef = FirebaseFirestore.instance.collection('Users');
    final snapshot = await usersRef.where('email', isEqualTo: email).get();

    return snapshot.docs.isNotEmpty;
  }

}


class RegistroUser {

  late String nombre, apellido, nickname, email, pwd1, pwd2;

  RegistroUser(){}
  void setParam(String nombre, String ape, String nick, String email, String pwd1, String pwd2){
    this.nombre=nombre;
    this.apellido=ape;
    this.nickname=nick;
    this.email=email;
    this.pwd1=pwd1;
    this.pwd2=pwd2;
  }


  Future<bool?> register() async {

      try {
        UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(email: email, password: pwd1);

        User? user = (await FirebaseAuth.instance.fetchSignInMethodsForEmail(email)).isEmpty
            ? null
            : FirebaseAuth.instance.currentUser;
        if(user != null) {
          String uid = user.uid;
          //creacion del documento
          final Map<String, dynamic> docu = {
            'name': nombre,
            'surname': apellido,
            'nickname': nickname,
            'email': email,
            'pwd': pwd1
          };
          //sentencia insert
          await collectionReference.doc(uid).set(docu);
          //cambiar pantalla
          return true;
        }
        return false;
      } on FirebaseAuthException catch (e) {
        if (e.code == 'weak-password') {
          return false;
        } else if (e.code == 'email-already-in-use') {
          return false;
        }
      } catch (e) {
        return false;
      }
    }

}