import 'package:firebase_auth/firebase_auth.dart';
import 'package:instaevents/mvvm/view/profileOne.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

FirebaseFirestore db = FirebaseFirestore.instance;
String? currentUserId;

class ComprobarCamposLogin {
  ComprobarCampos() {}

  Future<bool> checkEmailExists(String email) async {
    final usersRef = FirebaseFirestore.instance.collection('Users');
    final snapshot = await usersRef.where('email', isEqualTo: email).get();

    return snapshot.docs.isNotEmpty;
  }

  Future<bool> register(String email, String pwd, BuildContext context) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: pwd,
      );
      currentUserId = FirebaseAuth.instance.currentUser!.uid;
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ProfileOne()),
      );
    } on FirebaseAuthException catch (e) {
      if ( e.code == 'user-not-found' ) {
        return false;
      } else if (e.code == 'wrong-password') {
        return false;
      }
    }
    return true;
  }



}
