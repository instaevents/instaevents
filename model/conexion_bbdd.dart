import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';

class AbrirBBDD {
  AbrirBBDD() {}

  void conectarse() async {
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp();
  }
}