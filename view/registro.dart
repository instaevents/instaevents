import 'package:flutter/material.dart';

import 'package:instaevents/mvvm/logic/comprobarRegistro.dart';
import 'package:instaevents/mvvm/view/busqueda.dart';
//import 'package:flutter/cupertino.dart';

class Registro extends StatelessWidget{
  Registro({super.key});

  Widget formularioRegistroFuncional = const Form(
      child: MyCustomForm()
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            formularioRegistroFuncional
          ],
        ),
      ),
    );
  }
}



class MyCustomForm extends StatefulWidget {
  const MyCustomForm({super.key});

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {

  //Variables Maestras para cada Campo de texto
  final _nombreKey = GlobalKey<FormState>();
  final _nicknameKey = GlobalKey<FormState>();
  final _emailKey = GlobalKey<FormState>();
  final _pwdKey = GlobalKey<FormState>();

  //Variables recoger el valor que ha introducido el usuario
  final nombreCtrl = TextEditingController();
  final apellidosCtrl = TextEditingController();
  final nicknameCtrl = TextEditingController();
  final emailCtrl = TextEditingController();
  final pwd1Ctrl = TextEditingController();
  final pwd2Ctrl = TextEditingController();

  ComprobarCamposRegistro validCampos = ComprobarCamposRegistro();
  RegistroUser user = RegistroUser();//nombreCtrl.text, apellidosCtrl.text, nicknameCtrl.text, emailCtrl.text, pwd1Ctrl.text, pwd2Ctrl.text
  
  bool _existsNickname = false;
  bool _existsEmail = false;

  bool siguienteVista = false;
  void registro() async {

    user.setParam(nombreCtrl.text, apellidosCtrl.text, nicknameCtrl.text, emailCtrl.text, pwd1Ctrl.text, pwd2Ctrl.text);
    siguienteVista = (await user.register())!;
    if(siguienteVista){
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Busqueda()),
      );
    }
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
      child: Column(
        children: [
          Form(key: _nombreKey, child: Row(
            children: [
              Expanded(
                  child: Container(
                      padding: const EdgeInsets.fromLTRB(0, 10, 5, 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextFormField(
                            decoration: const InputDecoration(
                              hintText: "Nombre"
                            ),
                            controller: nombreCtrl,
                            validator: (value) {
                              if( value==null || value.isEmpty ){
                                return "Introduce un nombre";
                              }
                              return null;
                            },
                          )
                        ],
                      )
                  )
              ),
              Expanded(
                  child: Container(
                      padding: const EdgeInsets.fromLTRB(5, 10, 0, 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextFormField(
                            decoration: const InputDecoration(
                                hintText: "Apellidos"
                            ),
                            controller: apellidosCtrl,
                            validator: (value) {
                              if( value==null || value.isEmpty ){
                                return "Introduce un apellido";
                              }
                              return null;
                            },
                          )
                        ],
                      )
                  )
              ),
            ],
          )),
          Form(key: _nicknameKey, child: Row(
              children: [
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextFormField(
                          decoration: const InputDecoration(
                              hintText: "Nick_name (Obligatorio una (_) )"
                          ),
                          controller: nicknameCtrl,
                          validator: (value) {
                            if( value==null || value.isEmpty ){
                              return "Introduce un nickname";
                            }
                            if( !value.contains("_") ){
                              return "Debe contener un _";
                            }
                            if( _existsNickname ){
                              return "Nickname ya registrado";
                            }
                            return null;
                          },
                          onChanged: (value) async {
                            _existsNickname = await validCampos.checkNicknamelExists(nicknameCtrl.text);
                          }
                        )
                      ],
                    )
                  )
                )])),
          Form(key: _emailKey, child: Row(
             children: [
                Expanded(
                    child: Container(
                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextFormField(
                              decoration: const InputDecoration(
                                  hintText: "Correo electronico"
                              ),
                              controller: emailCtrl,
                              validator: (value) {
                                if( value==null || value.isEmpty ){
                                  return "Introduce un correo electronico";
                                }
                                if( !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value) ){
                                  return "Introduce un correo valido";
                                }
                                if( _existsEmail ){
                                  return "Email ya registrado";
                                }
                                return null;
                              },
                              onChanged: (value) async {
                                _existsEmail = await validCampos.checkEmailExists(emailCtrl.text);
                              },
                            )
                          ],
                        )
                    )
                )])),
          Form(key: _pwdKey, child: Row(
            children: [
              Expanded(
                  child: Container(
                      padding: const EdgeInsets.fromLTRB(0, 10, 5, 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextFormField(
                            decoration: const InputDecoration(
                                hintText: "Contraseña"
                            ),
                            controller: pwd1Ctrl,
                            validator: (value) {
                              if( value==null || value.isEmpty ){
                                return "Introduce una contraseña";
                              }
                              if( value.length<8 ){
                                return "Contraseña";
                              }
                            },
                          )
                        ],
                      )
                  )
              ),
              Expanded(
                  child: Container(
                      padding: const EdgeInsets.fromLTRB(5, 10, 0, 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextFormField(
                            decoration: const InputDecoration(
                                hintText: "Repita la contraseña"
                            ),
                            controller: pwd2Ctrl,
                            validator: (value) {
                              if( value==null || value.isEmpty ){
                                return "Introduce una contraseña";
                              }
                              if( value!=pwd1Ctrl.text ){
                                return "La contraseña no coincide";
                              }
                              return null;
                            },
                          )
                        ],
                      )
                  )
              ),
            ],
          )),
          ElevatedButton(
            onPressed: () async {
              _nombreKey.currentState!.validate();
              _nicknameKey.currentState!.validate();
              _emailKey.currentState!.validate();
              _pwdKey.currentState!.validate();



              if(
                nombreCtrl==null || nombreCtrl.text.isNotEmpty
                &&
                apellidosCtrl==null || apellidosCtrl.text.isNotEmpty
                &&
                nicknameCtrl==null || nicknameCtrl.text.isNotEmpty && nicknameCtrl.text.contains("_") && !_existsNickname
                &&
                emailCtrl==null || emailCtrl.text.isNotEmpty && RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailCtrl.text) && !_existsEmail
                &&
                pwd1Ctrl==null || pwd1Ctrl.text.isNotEmpty && pwd1Ctrl.text.length>8
                &&
                pwd2Ctrl==null || pwd2Ctrl.text.isNotEmpty && pwd1Ctrl.text==pwd2Ctrl.text
              ){
                registro();
              }

            },
            child: const Text('Siguiente.'),
          ),
        ],
      ),
    );
  }
}