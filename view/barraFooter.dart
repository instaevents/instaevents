import 'package:flutter/material.dart';
import 'package:instaevents/mvvm/view/busqueda.dart';
import 'package:instaevents/mvvm/view/crearEvento.dart';
import 'package:instaevents/mvvm/view/profileOne.dart';
import 'inicio.dart';

class BarraNavegacionN extends StatelessWidget{
  const BarraNavegacionN({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          border: Border(
              top: BorderSide(color: Colors.grey)
          )
      ),
      child: Row(
        children: [
          Expanded(child: Home()),
          Expanded(child: NuevoEvento()),
          Expanded(child: Search()),
          Expanded(child: Profile())
        ],
      ),
    );
  }
}

class Home extends StatelessWidget{
  Home({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Inicio())
        );
      },
      child: const Icon(
        Icons.home,
        color: Colors.black,
        size: 40,
      ),
    );
  }
}

class Search extends StatelessWidget{
  Search({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Busqueda())
        );
      },
      child: const Icon(
        Icons.home,
        color: Colors.black,
        size: 40,
      ),
    );
  }
}

class NuevoEvento extends StatelessWidget{
  NuevoEvento({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CreateEvent())
        );
      },
      child: const Icon(
        Icons.home,
        color: Colors.black,
        size: 40,
      ),
    );
  }
}


class Profile extends StatelessWidget{
  Profile({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ProfileOne())
        );
      },
      child: const Icon(
        Icons.home,
        color: Colors.black,
        size: 40,
      ),
    );
  }
}