import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'barraFooter.dart';
import 'busqueda.dart';
import 'busqueda_usr.dart';
import 'inicio.dart';
import 'iniciomain.dart';
import 'profileOne.dart';
import 'recoveryPassw.dart';
import 'recoveryPasswSecond.dart';


class ProfileOne extends StatelessWidget {
  const ProfileOne({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Body(),
    );
  }
}

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: ClaseLlamada(),
        ),
      bottomNavigationBar: BarraNavegacionN(),
    );
  }
}

class ClaseLlamada extends StatelessWidget {
  const ClaseLlamada({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
            RowOne(),
          SizedBox(
            //Use of SizedBox
            height: 10,
          ),
            RowTwo(),
            Container(
              margin: EdgeInsets.only(top: 20),
              color: Colors.grey,
              height: 600,
            )
        ],
      );
  }
}

class RowOne extends StatelessWidget {
  const RowOne({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
        children: [
          Expanded(child: ColumnOne()),
          Expanded(child: ColumnTwo()),
          Expanded(child: ColumnThree()),
        ],
      );
  }
}

class ColumnOne extends StatelessWidget {
  const ColumnOne({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 50),
            child: Text("0"),
          ),
          Container(
            child: Text("Seguidores"),
          )
        ],
      );
  }
}


class ColumnTwo extends StatelessWidget {
  const ColumnTwo({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50, // Establece la altura deseada del Container
      margin: EdgeInsets.only(top: 45),
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          final double imageHeight = constraints.maxHeight / 5;
          final double imageWidth = constraints.maxHeight / 5;
          return Image.asset(
            'assets/iconouser.jpg',
            width: imageWidth,
            height: imageHeight,
          );
        },
      ),
    );
  }
}


class ColumnThree extends StatelessWidget {
  const ColumnThree({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 50),
            child: Text("0"),
          ),
          Container(
            child: Text("Seguidos"),
          )
        ],
      );
  }
}

class RowTwo extends StatelessWidget {
  const RowTwo({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(child: PrimeraImagen()),
        Expanded(child: SegundaImagen()),
      ],
    );
  }
}

class PrimeraImagen extends StatelessWidget {
  const PrimeraImagen();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40, // Establece la altura deseada del Container
      margin: EdgeInsets.only(top: 40),
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          final double imageHeight = constraints.maxHeight / 2;
          return Image.asset(
            'assets/iconoCuadricula.jpg',
            width: imageHeight,
            height: imageHeight,
          );
        },
      ),
    );
  }
}

class SegundaImagen extends StatelessWidget {
  const SegundaImagen();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40, // Establece la altura deseada del Container
      margin: EdgeInsets.only(top: 40),
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          final double imageHeight = constraints.maxHeight / 2;
          return Image.asset(
            'assets/iconomapa.png',
            width: imageHeight,
            height: imageHeight,
          );
        },
      ),
    );
  }
}


class BarraNavegacion extends StatelessWidget{
  const BarraNavegacion({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: const [
        Expanded(child: Icon(Icons.home)),
        Expanded(child: Icon(Icons.add)),
        Expanded(child: Icon(Icons.person)),
      ],
    );
  }
}


