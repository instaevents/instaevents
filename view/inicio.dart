import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'barraFooter.dart';
import 'busqueda_usr.dart';


class Inicio extends StatelessWidget{
  const Inicio({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home()
    );
  }
}

class Home extends StatelessWidget{
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: const Text("InstaEvents"),
      ),
      body: ListaTodosEventos(),
      bottomNavigationBar: BarraNavegacionN(),
    );
  }
}

class ListaTodosEventos extends StatelessWidget{
  const ListaTodosEventos({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CajonEventos()
      ],
    );
  }
}

class CajonEventos extends StatelessWidget{
  const CajonEventos({super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                ConjuntoEvento(),
                ConjuntoEvento(),
                ConjuntoEvento(),
                ConjuntoEvento(),
                ConjuntoEvento(),
                ConjuntoEvento(),
                ConjuntoEvento(),
                ConjuntoEvento(),
                ConjuntoEvento(),
              ],
            ),
          ),
        )
    );
  }
}

class ConjuntoEvento extends StatelessWidget{
  const ConjuntoEvento({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 25, 10, 5),
      child: Column(
        children: const [
          UnUser(),
          Posicion()
        ],
      ),
    );
  }

}

class Posicion extends StatelessWidget{
  const Posicion({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      //width: 200, height: 100,
      margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
        child: const Image(
          image: NetworkImage('https://imagenes.20minutos.es/files/image_990_v3/uploads/imagenes/2010/02/23/1047981a.jpg'),
        )
    );
  }

}
