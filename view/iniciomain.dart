import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:instaevents/mvvm/logic/comprobarEmailRecovery.dart';
import 'package:instaevents/mvvm/logic/comprobarLogin.dart';
import 'package:instaevents/mvvm/view/recoveryPassw.dart';
import 'package:instaevents/mvvm/view/registro.dart';

class Login extends StatelessWidget{
  Login({super.key});

  Widget formularioRegistroFuncional = const Form(
      child: MyCustomForm()
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SingleChildScrollView (
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              formularioRegistroFuncional
            ],
          ),
        ),
      ),
    );
  }
}

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({super.key});

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {

  //Variables Maestras para cada Campo de texto
  final _emailKey = GlobalKey<FormState>();
  final _pwdKey = GlobalKey<FormState>();

  //Variables recoger el valor que ha introducido el usuario
  final emailCtrl = TextEditingController();
  final pwd1Ctrl = TextEditingController();

  //instanciar la clase ComprobarCampoLogin
  ComprobarCamposLogin validCampos = ComprobarCamposLogin();

  bool _emailExists = false;

  bool siguientePaso = false;
  void preuba() async {//cambiar el nombre
    bool anterior = await validCampos.register(emailCtrl.text, pwd1Ctrl.text, context);
    setState(() {
      siguientePaso = anterior;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget espacio_msgErr = siguientePaso ? Container(height: 25, child: Text("Datos incorrectos"),) : SizedBox(height: 25,);
    return Container(
      padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
            child: Text("LOGIN",  style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
          ),
          Form(key: _emailKey, child: Row(
              children: [
                Expanded(
                    child: Container(
                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextFormField(
                              decoration: const InputDecoration(
                                  hintText: "Correo electronico"
                              ),
                              controller: emailCtrl,
                              validator: (value) {
                                if( value==null || value.isEmpty ){
                                  return "Por favor introduce un correo electrónico";
                                }else if( !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value) ){
                                  return "Por favor introduce un correo electrónico valido";
                                }else if( !_emailExists ){
                                  return "Por favor introduce un correo electrónico registrado";
                                }
                                return null;
                              },
                              onChanged: (value) async {
                                _emailExists = await validCampos.checkEmailExists(value);
                              },
                            )
                          ],
                        )
                    )
                )])),
          Form(key: _pwdKey, child: Row(
            children: [
              Expanded(
                  child: Container(
                      padding: const EdgeInsets.fromLTRB(0, 10, 5, 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextFormField(
                            decoration: const InputDecoration(
                                hintText: "Contraseña"
                            ),
                            controller: pwd1Ctrl,
                            obscureText: true,
                            validator: (value) {
                              if( value==null || value.isEmpty){
                                return "Introduce una contraseña";
                              }
                              return null;
                            },
                          )
                        ],
                      )
                  )
              ),
            ],
          )),
          InkWell(
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => RecoveryOne()),
              );
            },
            child: Text("Recuperar contraseña"),
          ),
          espacio_msgErr,
          ElevatedButton(
            onPressed: () {
              _emailKey.currentState!.validate();
              _pwdKey.currentState!.validate();

              //siguientePaso = validCampos.register(emailCtrl.text, pwd1Ctrl.text, context) as bool;
              preuba();
            },
            child: const Text('Next'),
          ),
          ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Registro()),
                );
              },
              child: const Text("Registro"))
        ],
      ),
    );
  }
}



