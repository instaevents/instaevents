import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'busqueda.dart';
import 'busqueda_usr.dart';
import 'inicio.dart';
import 'iniciomain.dart';
import 'profileOne.dart';
import 'recoveryPassw.dart';
import 'recoveryPasswSecond.dart';


class ListaUsers extends StatelessWidget{
  const ListaUsers({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: ListaTodosUsers()
      ),
    );
  }
}

class ListaTodosUsers extends StatelessWidget{
  const ListaTodosUsers({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        CajonBusqueda(),
        CajonUsuarios()
      ],
    );
  }
}

class CajonUsuarios extends StatelessWidget{
  const CajonUsuarios({super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: const EdgeInsets.fromLTRB(10, 25, 10, 5),
        child: SingleChildScrollView(
          child: Column(
            children: const [
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
              UnUser(),
            ],
          ),
        ),
      ),
    );
  }

}


class CajonBusqueda extends StatelessWidget{
  const CajonBusqueda({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 25, 10, 0),
      child: const CupertinoTextField(),
    );
  }
}


class UnUser extends StatelessWidget{
  const UnUser({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 10, 10, 0),
      child: const ImagenMasNombre()
    );
  }
}

class ImagenMasNombre extends StatelessWidget{
  const ImagenMasNombre({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: const [
        ContainerImagenUsuario(),
        Expanded(child: Text("simon__ss0"))
      ],
    );
  }
}

class ContainerImagenUsuario extends StatelessWidget{
  const ContainerImagenUsuario({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
      child: const SizedBox(width: 50, child: ImagenUsuario()),
    );
  }

}

class ImagenUsuario extends StatelessWidget{
  const ImagenUsuario({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: const [
       Image(height: 50, width: 50,
              image: NetworkImage('https://instagram.fmad11-1.fna.fbcdn.net/v/t51.2885-19/326392011_1095731347763420_3641723239207495013_n.jpg?stp=dst-jpg_s320x320&_nc_ht=instagram.fmad11-1.fna.fbcdn.net&_nc_cat=108&_nc_ohc=qnAF21BPkJkAX_R0Rly&edm=AOQ1c0wBAAAA&ccb=7-5&oh=00_AfBiHy9k7AgOrxazgiFL_VTFhFhPcKKQD_BHBcS1OMYnSg&oe=63F12CF6&_nc_sid=8fd12b')),
      ],
    );
  }
}