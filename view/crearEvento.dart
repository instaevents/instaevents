/*import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CreateEvent extends StatelessWidget{
  CreateEvent({super.key});

  Widget formularioCreateEvent = Form(
      child: FormCreateEvent()
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      home: Scaffold(
        body: Container(
          padding: const EdgeInsets.fromLTRB(25, 50, 25, 15),
          child: formularioCreateEvent,
        ),
      ),
    );
  }
}


class FormCreateEvent extends StatefulWidget{
  FormCreateEvent({super.key});

  @override
  State<StatefulWidget> createState() {
    return FormCreateEventState();
  }
}

class FormCreateEventState extends State<FormCreateEvent>{

  final titleCtrl = TextEditingController();
  final srchCtrl = TextEditingController();
  final descrCtrl = TextEditingController();

  Widget espacio = const SizedBox(height: 10,);
  Widget? title;
  Widget imagen = InkWell(
    //statesController: ,
    onTap: (){
      print("has pulksado es un buen sitio");
    },
    child: Container(
      height: 100,
      width: double.infinity,
      decoration: const BoxDecoration(
        color: Colors.orange
      ),
      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Text("Elija una imagen"),
    ),
  );
  Widget? buscadorUbicacion, descripcion;

  //https://dev.to/oscar__martin/datepicker-para-formularios-en-flutter-3g9f
  Widget selecionadorFecha(BuildContext Context){return TextButton(onPressed: (){
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(2025)
    );
  }, child: Text("Selecciona una fecha"));}
  */

  /*
  en el widget de buscador de ubicacion poner una condicion de:
  if(hayInternet()){
    autocompletegoogleApi
    }else{
    sizebox o una alerta de algun tipo
    }



  @override
  void initState(){
    super.initState();
    title = CupertinoTextField(
      controller: titleCtrl,
      placeholder: "Titulo *",
    );
    buscadorUbicacion = CupertinoTextField(
      controller: srchCtrl,
      placeholder: "Buscador *",
    );
    descripcion = CupertinoTextField(
      controller: descrCtrl,
      placeholder: "Descripcion",
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        title!,   //Explicacion en Notas>Recursos>titleNULL
        espacio,
        imagen,
        espacio,
        buscadorUbicacion!,
        espacio,
        descripcion!,
        espacio,
        selecionadorFecha(context)
      ],
    );
  }
}
*/
  /*
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(padding: EdgeInsets.fromLTRB(25, 25, 25, 25), child: Column(
          //eje arriba-abajo (y)
          mainAxisAlignment: MainAxisAlignment.center,
          //eje derecha-izquierda (x)
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Top"),
            CambioElemento(),
            Text("Botton")
          ],
        )
      ),
    ));
  }
}

class CambioElemento extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return CambioElementoState();
  }
}

class CambioElementoState extends State<CambioElemento>{
  bool siguientePaso = true;
  int posi = 0;

  @override
  Widget build(BuildContext context) {
    Widget pruebaaaa = siguientePaso?ElevatedButton(onPressed: (){}, child: Text("ee")):Text("Falso");
    return Row(
      children: [
        Expanded(child: pruebaaaa),
        Expanded(child: ElevatedButton(

          onPressed: (){
            setState(() {
              posi++;
              siguientePaso = posi % 2 == 0 ? true : false;
            });
          }, child: Text("Change"),
        ))
      ],
    );
  }
}*/
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';

import 'package:instaevents/mvvm/logic/comprobarLogin.dart';

class CreateEvent extends StatelessWidget{
  CreateEvent({super.key});

  Widget formularioCreateEvent = Form(
      child: FormCreateEvent()
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      home: Scaffold(
        body: Container(
          padding: const EdgeInsets.fromLTRB(25, 50, 25, 15),
          child: formularioCreateEvent,
        ),
      ),
    );
  }
}


class FormCreateEvent extends StatefulWidget{
  FormCreateEvent({super.key});

  @override
  State<StatefulWidget> createState() {
    return FormCreateEventState();
  }
}

class FormCreateEventState extends State<FormCreateEvent>{

  final titleCtrl = TextEditingController();
  final srchCtrl = TextEditingController();
  final descrCtrl = TextEditingController();
  File? image;
  DateTime? fechaSeleccionada;

  Widget espacio = const SizedBox(height: 10,);
  Widget? title;
  Widget? imagenSeleccion;
  Widget? imagenBorrar;
  Widget? buscadorUbicacion, descripcion;
  Widget? CrearEventButton;

  //https://dev.to/oscar__martin/datepicker-para-formularios-en-flutter-3g9f
  Widget selecionadorFecha(BuildContext context) {
    return TextButton(
      onPressed: () {
        showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime(2020),
          lastDate: DateTime(2025),
        ).then((fecha) {
          if (fecha != null) {
            setState(() {
              fechaSeleccionada = fecha;
            });
          }
        });
      },
      child: Text("Selecciona una fecha"),
    );
  }

  /*
  en el widget de buscador de ubicacion poner una condicion de:
  if(hayInternet()){
    autocompletegoogleApi
    }else{
    sizebox o una alerta de algun tipo
    }
   */

  @override
  void initState(){
    super.initState();
    title = CupertinoTextField(
      controller: titleCtrl,
      placeholder: "Titulo *",
    );
    buscadorUbicacion = CupertinoTextField(
      controller: srchCtrl,
      placeholder: "Buscador *",
    );
    descripcion = CupertinoTextField(
      controller: descrCtrl,
      placeholder: "Descripcion",
    );
    imagenBorrar =  InkWell(
      //statesController: ,
      onTap: () {
        DevolverEstado();
      },
      child: Container(
        height: 20,
        width: double.infinity,
        decoration: const BoxDecoration(
            color: Colors.grey
        ),
        child: Text("Eliminar imágen"),
      ),
    );
    imagenSeleccion = InkWell(
      //statesController: ,
      onTap: _pickImage,
      child: Container(
        height: 300,
        width: double.infinity,
        decoration: const BoxDecoration(
            color: Colors.orange
        ),
        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Text("Elija una imagen"),
      ),
    );
    CrearEventButton = ElevatedButton(
      onPressed: () {
        subirEvento(titleCtrl, descrCtrl, srchCtrl, fechaSeleccionada!, image!);
      },
      child: Text('Presioname'),
    );
  }

  Future<void> _pickImage() async {
    final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        image = File(pickedFile.path);
        imagenSeleccion = Image.file(image!);
      });
    }
  }

  void DevolverEstado() {
    setState(() {
      image = null;
      imagenSeleccion = InkWell(
        //statesController: ,
        onTap: _pickImage,
        child: Container(
          height: 300,
          width: double.infinity,
          decoration: const BoxDecoration(
              color: Colors.orange
          ),
          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
          child: Text("Elija una imagen"),
        ),
      );
    });
  }

  void subirEvento(TextEditingController titleCtrl, TextEditingController descrCtrl, TextEditingController srchCtrl, DateTime fechaSeleccionada, File image) async {
    String? usuarioIniciado = currentUserId;

    final eventsRef = FirebaseFirestore.instance.collection('Events');
    final fileName = DateTime.now().millisecondsSinceEpoch.toString();
    final imageRef = FirebaseStorage.instance.ref().child('images/$fileName');
    final eventData = {
      'title': titleCtrl.text,
      'description': descrCtrl.text,
      'date': fechaSeleccionada,
      //'id_user' : usuarioIniciado,
      'image_url': '', // Esta será la URL de la imagen una vez que se haya subido a Firebase Storage
    };
    final task = imageRef.putFile(image);
    task.then((snapshot) async {
      final imageUrl = await imageRef.getDownloadURL();
      eventData['image_url'] = imageUrl;
      await eventsRef.add(eventData);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column (
          children: [
            title!,   //Explicacion en Notas>Recursos>titleNULL
            espacio,
            imagenSeleccion!,
            espacio,
            imagenBorrar!,
            espacio,
            //buscadorUbicacion!,
            espacio,
            descripcion!,
            espacio,
            selecionadorFecha(context),
            espacio,
            CrearEventButton!,
          ],
        )
    );
  }
}

